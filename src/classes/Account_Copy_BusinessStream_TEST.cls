@isTest
private class Account_Copy_BusinessStream_TEST {

    static testMethod void AccountBUsinessStreamtTest() {
        //Create and insert an account record with no business stream value set
        Account newAcc = new Account(Name = 'Davids TEST ORG', site = 'LONDON HQ', BillingCity = 'London');
        insert newAcc;
        
        //TEST 1
        //query back the inserted account and assert that the trigger has correctly copied across the business stream values
        Account checkAcc = new Account(); 
        checkAcc = [select id, name, Business_stream_text__c, Business_Stream__c from Account where id = :newAcc.id];
        system.AssertEquals(checkAcc.Business_Stream_Text__c, checkAcc.Business_Stream__c);
        
        //update the account with a valie for business stream 
        newAcc.Business_Stream__c = 'ELT;AcPro'; // set the Business Stream to something
        update newAcc;
        
        //TEST 2
        //query back the updated account and assert that the trigger has correctly copied across the business stream values
        Account checkAcc2 = new Account();
        checkAcc2 = [select id, name, Business_stream_text__c, Business_Stream__c from Account where id = :newAcc.id];
        system.AssertEquals(checkAcc2.Business_Stream_Text__c, checkAcc2.Business_Stream__c);
        
        //TEST 3
        //query back the account again, without making an update, to hit other branch to aim for 100% coverage
        Account checkAcc3 = new Account();
        checkAcc3 = [select id, name, Business_stream_text__c, Business_Stream__c from Account where id = :newAcc.id];
        system.AssertEquals(checkAcc3.Business_Stream_Text__c, checkAcc3.Business_Stream__c);
        
    }
}